#!/usr/bin/python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup

import urllib, urlparse

########### ##############

url  = "http://www.blt.se/solvesborg"
page = urllib.urlopen(url)

soup = BeautifulSoup(page.read())
# section = soup.select(".item-list")


links = soup.select(".art-title")

for link in links:
	# all links to news article on page
	#print link.contents[1]['href']
	article_url = link.contents[1]['href']

	print article_url
	html = urllib.urlopen(article_url)
	article_soup = BeautifulSoup(html.read())
	
	article_title = article_soup.select(".art-content")
	article_title = article_title[0].contents[3].select(".caption")[0].contents[0]
	print article_title

	p_elements = article_soup.select(".e-content")[0].select("p")
	for p in p_elements:
		print p
	#print article_content
	break
	#print article_soup

#print section
#print links